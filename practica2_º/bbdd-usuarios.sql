USE Educamos;
DROP USER IF EXISTS 'admin'@'localhost';
DROP USER IF EXISTS 'consultor'@'localhost';
DROP USER IF EXISTS 'profe'@'localhost';

FLUSH PRIVILEGES;

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin1234';
GRANT ALL PRIVILEGES ON Educamos.* TO 'admin'@'localhost';

CREATE USER 'consultor'@'localhost' IDENTIFIED BY 'educamos';
GRANT SELECT ON Educamos.* TO 'consultor'@'localhost';

CREATE USER 'profe'@'localhost' IDENTIFIED BY 'secretpass';
GRANT SELECT, DELETE, INSERT, UPDATE ON Educamos.* TO 'profe'@'localhost';

FLUSH PRIVILEGES;
