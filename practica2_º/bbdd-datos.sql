USE Educamos;

/*Centro*/
INSERT INTO CENTRO VALUES ('SantaEnseñanza',                 'C/albarracinn78',      965574845, 349),
                          ('santo domingo savio',            'avenida enseñanza',    968757647, 636),
                          ('Salesianos Estrecho',            'C/cualquiera me vale', 924173175, 567),
                          ('Colegio Salesianos Carabanchel', 'c/San vicente 1',      963903973, 321),
                          ('Salesianos Arcanjel',            'Avenida del señor',    918346002, 145);
                          SELECT * FROM CENTRO;
/*Seguridad*/
INSERT INTO SEGURIDAD VALUES ('C.Martinez',  '2015.carlos'),
                             ('I.Sanchez',   '2015.Isabel'),
                             ('S.Garrido',   '2015.Santiago'),
                             ('F.Palacios',  '2015.Fabian'),
                             ('E.Carbajal',  '2015.Erick'),

                             ('R.Martinez',  '2015.Ramon' ),
                             ('D.Truan',     '2015.Diana'),
                             ('S.Palacios',  '2015.Santiago'),
                             ('D.Garrido',   '2015.Diego'),
                             ('Z.Carbajal',  '2015.Zuley'),

                             ('M.Prado',     '2015.Miguel'),
                             ('A.Sanchez',   '2015.Alba'),
                             ('L.Estelar',   '2015.Lucas'),
                             ('S.Nuñes',     '2015.Sofia'),
                             ('A.Maduro',    '2015.Angel');
                             SELECT * FROM SEGURIDAD;
/*Persona*/
INSERT INTO PERSONA VALUES                                  /*Alumnos*/
                           ('Carlos',   'Martinez Santos',  '07097620A', 987473737, 'H', '1999-12-27', 145),
                           ('Isabel',   'Sanchez Ramirez',  '06005273Y', 963424362, 'M', '1996-02-23', 145),
                           ('Fabian',   'Palacios Soto',    '02342342Q', 913742782, 'H', '1998-01-10', 145),
                           ('Santiago', 'Garrido Real',     '06882342N', 912782522, 'H', '1997-12-23', 145),
                           ('Erick',    'Carbajal Calvo',   '07283642L', 924646464, 'H', '1998-12-01', 145),
                                                            /*Familiares*/
                           ('Ramón',    'Martinez Lodín',   '07234234P', 913413451, 'H', '1970-02-21', 145),
                           ('Diana',    'Truan Sanchez',    '06001234O', 982345984, 'M', '1971-12-30', 145),
                           ('Sara',     'Palacios Dardo',   '07986757K', 984672244, 'M', '1973-06-22', 145),
                           ('Diego',    'Garrido Coto',     '06456788U', 974643665, 'H', '1980-07-03', 145),
                           ('Zuley',    'Carbajal Perez',   '06363466E', 905366255, 'M', '1976-09-17', 145),
                                                            /*Profesores*/
                           ('Miguel',   'Prado Tijeras',    '06246773S', 923526422, 'H', '1975-08-01', 145),
                           ('Alba',     'Sanchez Castillo', '06002342Q', 946363363, 'H', '1987-01-23', 145),
                           ('Lucas',    'Estelar Prado',    '06723563X', 918654656, 'M', '1981-09-11', 145),
                           ('Sofía',    'Nuñes Soto',       '07346222F', 958356343, 'M', '1984-09-10', 145),
                           ('Angel',    'Maduro Castillo',  '07003453D', 936472272, 'H', '1975-07-02', 145),
                           ('Rui',      'Perez Castillo',   '07022453C', 936262771, 'H', '1976-07-02', 145);
                           SELECT * FROM PERSONA;

/*Evento*/
INSERT INTO EVENTO VALUES ('Carlos', 'Martinez Santos', '2018-12-27', 'e72'),
                          ('Isabel', 'Sanchez Ramirez', '2018-02-23', 'd-38'),
                          ('Fabian', 'Palacios Soto',   '2018-01-10', 'd-36'),
                          ('Santiago', 'Garrido Real',  '2018-12-24', 'e-72'),
                          ('Erick', 'Carbajal Calvo',   '2018-03-15', 'e-70');
                          SELECT * FROM EVENTO;

/*Avisos*/
INSERT INTO AVISOS VALUES ('Miguel', 'Prado Tijeras',     'Reunion de profesores dia 24, a las 14:00 salon actos'),
                          ('Alba',   'Sanchez Castillo',  'ninguno'),
                          ('Lucas',  'Estelar Prado',     'Reunion de profesores dia 24, a las 14:00 salon actos'),
                          ('Sofía',  'Nuñes Soto',        'Reunion de profesores dia 24, a las 14:00 salon actos'),
                          ('Angel',  'Maduro Castillo',   'Reunion de profesores dia 24, a las 14:00 salon actos');
                          SELECT * FROM AVISOS;

/*Deberes*/
INSERT INTO DEBERES VALUES ('Carlos', 'Martinez Santos',  'practica bbdd dia 28 de febrero'),
                           ('Isabel', 'Sanchez Ramirez',  'ninguno'),
                           ('Fabian', 'Palacios Soto',    'ninguno'),
                           ('Santiago', 'Garrido Real',   'ninguno'),
                           ('Erick', 'Carbajal Calvo',    'Ejercicios pag141, 1-5');
                           SELECT * FROM DEBERES;

/*Curso*/
INSERT INTO CURSO VALUES  ('CD345', 'Desarrollo de aplicaciones multiplataforma', 'IDE0023', 'GS'),
                          ('CD346', 'Mecanizado',                                 'IDE0024', 'GS'),
                          ('CM447', 'Teleco',                                     'IDE0125', 'GM'),
                          ('CM448', 'Electronica',                                'IDE0126', 'GM'),
                          ('SS012', '2º Bachillerato',                            'IDE0012', 'Bachiller'),
                          ('SS011', '1º Bachillerato',                            'IDE0011', 'Bacheller');
                          UPDATE CURSO SET nombre_CURSOgeneral='Bachiller' WHERE nombre_CURSOgeneral='Bacheller';
                          SELECT * FROM CURSO;
/*Alumno*/
INSERT INTO ALUMNO VALUES ('Carlos',   'Martinez Santos', 23,  'CD345', 'Desarrollo de aplicaciones multiplataforma'),
                          ('Isabel',   'Sanchez Ramirez', 15,  'CD346', 'Mecanizado'),
                          ('Fabian',   'Palacios Soto',   14,  'CM447', 'Teleco'),
                          ('Santiago', 'Garrido Real',    29,  'CM448', 'Electronica'),
                          ('Erick',    'Carbajal Calvo',   1,  'SS012', '2º Bachillerato');
                          SELECT * FROM ALUMNO;


/*Familiar*/
INSERT INTO FAMILIAR VALUES ('Ramón',  'Martinez Lodín',  'Charlas sobre drogas'),
                            ('Diana',  'Truan Sanchez',   'Voluntaria en la parroquia'),
                            ('Sara',   'Palacios Dardo',  'ninguna'),
                            ('Diego',  'Garrido Coto',    'ninguna'),
                            ('Zuley',  'Carbajal Perez',  'ninguna');
                            SELECT * FROM FAMILIAR;

/*Supervisa*/
INSERT INTO SUPERVISA VALUES ('Ramón',  'Martinez Lodín', 'Carlos',   'Martinez Santos'),
                             ('Diana',  'Truan Sanchez',  'Isabel',   'Sanchez Ramirez'),
                             ('Sara',   'Palacios Dardo', 'Fabian',   'Palacios Soto'),
                             ('Diego',  'Garrido Coto',   'Santiago', 'Garrido Real'),
                             ('Zuley',  'Carbajal Perez', 'Erick',    'Carbajal Calvo');
                            SELECT * FROM SUPERVISA;
/*Profesor*/
INSERT INTO PROFESOR VALUES ('Miguel', 'Prado Tijeras',    'Base de datos'),
                            ('Alba',   'Sanchez Castillo', 'manejo de maquinaria'),
                            ('Lucas',  'Estelar Prado',    'señales de radio frecuencia'),
                            ('Sofía',  'Nuñes Soto',       'Soldaduras y mecanismos'),
                            ('Angel',  'Maduro Castillo',  'Matematicas');
                            SELECT * FROM PROFESOR;

/*Imparte*/
INSERT INTO IMPARTE VALUES ('Miguel', 'Prado Tijeras',    'Desarrollo de aplicaciones multiplataforma', 'CD345'),
                           ('Alba',   'Sanchez Castillo', 'Mecanizado',                                 'CD346'),
                           ('Lucas',  'Estelar Prado',    'Teleco',                                     'CM447'),
                           ('Sofía',  'Nuñes Soto',       'Electronica',                                'CM448'),
                           ('Angel',  'Maduro Castillo',  '2º Bachillerato',                            'SS012'),
                           ('Angel',  'Maduro Castillo',  '1º Bachillerato',                            'SS011');
                           SELECT * FROM IMPARTE;

/*Asignatura*/
INSERT INTO ASIGNATURA VALUES ('MD001', 'Matematicas aplicadas I'),
                              ('MD002', 'Matematicas aplicadas II'),
                              ('AS001', 'Bases de datos'),
                              ('AS002', 'Calderas y refrigeracion'),
                              ('AS003', 'Soldadura'),
                              ('AS005', 'Automatismos'),
                              ('AS009', 'Electricidad fundamentos');
                              SELECT * FROM ASIGNATURA;

/*Tiene*/
INSERT INTO TIENE VALUES ('Carlos',   'Martinez Santos',   'AS001', 'Bases de datos',           'Aula50'),
                         ('Isabel',   'Sanchez Ramirez',   'AS002', 'Calderas y refrigeracion', 'Taller1'),
                         ('Isabel',   'Sanchez Ramirez',   'AS003', 'Soldadura',                'Taller2'),
                         ('Fabian',   'Palacios Soto',     'AS005', 'Automatismos',             'Aula35'),
                         ('Santiago', 'Garrido Real',      'AS009', 'Electricidad fundamentos', 'Aula35'),
                         ('Erick',    'Carbajal Calvo',    'MD002', 'Matematicas aplicadas II', 'Aula 70'),
                         ('Erick',    'Carbajal Calvo',    'AS005', 'Automatismos',             'Aula35');
                         SELECT * FROM TIENE;

/*Telefonoi*/
INSERT INTO TELEFONO VALUES                                  /*Alumnos*/
                             ('Carlos',   'Martinez Santos',   687473737),
                             ('Isabel',   'Sanchez Ramirez',   663424362),
                             ('Fabian',   'Palacios Soto',     623423425),
                             ('Santiago', 'Garrido Real',      668823425),
                             ('Erick',    'Carbajal Calvo',    672836426),
                                                              /*Familiares*/
                             ('Ramón',    'Martinez Lodín',    672342341),
                             ('Diana',    'Truan Sanchez',     660012344),
                             ('Sara',     'Palacios Dardo',    679867576),
                             ('Diego',    'Garrido Coto',      664567889),
                             ('Zuley',    'Carbajal Perez',    663634660),
                                                              /*Profesores*/
                             ('Miguel',   'Prado Tijeras',     662467732),
                             ('Alba',     'Sanchez Castillo',  660023425),
                             ('Lucas',    'Estelar Prado',     667235632),
                             ('Sofía',    'Nuñes Soto',        673462227),
                             ('Angel',    'Maduro Castillo',   670034539),
                             ('Rui',      'Perez Castillo',    690224537);
                             SELECT * FROM TELEFONO;


